PROJECT = gps_to_udp
TEMPLATE = app

QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4) {
  QT += widgets
  DEFINES += HAVE_QT5
}

DEPENDPATH += .

INCLUDEPATH += qextserialport \
    threadcomport \
    . \
    ..

SUBDIRS = qextserialport
unix: {
    DEFINES = _TTY_POSIX_
    HEADERS += qextserialport/posix_qextserialport.h
    SOURCES += qextserialport/posix_qextserialport.cpp
}
win32: {
    DEFINES = _TTY_WIN_
    HEADERS += qextserialport/win_qextserialport.h
    SOURCES += qextserialport/win_qextserialport.cpp
}
BUILDDIR = build/
win32:BUILDDIR = $$join(BUILDDIR,,,win32)
unix:BUILDDIR = $$join(BUILDDIR,,,unix)
macx:BUILDDIR = $$join(BUILDDIR,,,macx)
UI_DIR = $${BUILDDIR}/ui
UIC_DIR = $${BUILDDIR}/uic
MOC_DIR = $${BUILDDIR}/moc
RCC_DIR = $${BUILDDIR}/rcc
OBJECTS_DIR = $${BUILDDIR}/obj

SOURCES += main.cpp \
    threadcomport/threadcomport.cpp \
    qextserialport/qextserialbase.cpp \
    qextserialport/qextserialport.cpp \
    mainwindow.cpp

HEADERS += \
    threadcomport/threadcomport.h \
    qextserialport/qextserialbase.h \
    qextserialport/qextserialport.h \    
    mainwindow.h

FORMS += \
    mainwindow.ui

RESOURCES += \
    myresurs.qrc
