#include <QtCore>
#include <QtGui>
#include <QFile>
#include <string>

#include <mainwindow.h>
#include <threadcomport.h>

MainWindow::MainWindow(QWidget *parent) : m_comPort(0), m_comPortThread(0),
    QMainWindow(parent), ui(new Ui::MainWindow)

{
    ui->setupUi(this);
    socket_port = 45450;

    PortSettings portsettings;
    portsettings.BaudRate = BAUD9600;
    portsettings.Parity = PAR_NONE;
    portsettings.FlowControl = FLOW_OFF;
    portsettings.DataBits = DATA_8;
    portsettings.StopBits = STOP_1;
    portsettings.Timeout_Millisec = 500;
    portsettings.Timeout_Sec = 0;

    QStringList list;
    for (int i=0; i < 5; i++) {
        list << "/dev/ttyS" + QString::number(i)
             << "/dev/ttyUSB" + QString::number(i);
    }
    ui->cmb_port_name->addItems(list);

    m_comPortThread = new QMainComThread(ui->cmb_port_name->currentText(), &portsettings);
    m_comPortThread->start();
    while (!m_comPort)
    {
        m_comPort = m_comPortThread->getPort();
        qApp->processEvents();
    }

    socket = new QUdpSocket(this);
    socket->bind(QHostAddress::Any,socket_port);

    connect(ui->but_save, SIGNAL(clicked()), this, SLOT(port_b_open()));
    connect(m_comPort, SIGNAL(binsPackGot(QByteArray)), this, SLOT(receiveMsgpack(QByteArray)));
    connect(ui->chBox, SIGNAL(stateChanged(int)), this, SLOT(SendNMEAfromFile(int)));

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(TimerSend()));

    int ch = getCheckSum("$GNRMC,102409.00,A,5545.61976,N,03745.86813,E,0.069,,190221,,,A"); //69
    //int ch = getCheckSum("$GNRMC,102417.00,A,5545.61977,N,03745.86824,E,0.110,,190221,,,A"); //6C
    //int ch = getCheckSum("$GNRMC,102206.00,A,5545.61821,N,03745.86727,E,0.139,,190221,,,A"); //6F

    QString chsum = "69";
    bool bStatus = false;
    uint chsum_true = chsum.toUInt(&bStatus, 16);
    qDebug() << chsum_true <<"_"<< ch << "_" << bStatus;

    // example to hex convertion
    //QString hex;
    //hex.setNum(decimal,16);
    //qDebug() << hex << endl;
}


void MainWindow::port_b_open()
{
        if (m_comPort->isOpen()) {
            m_comPort->close();
            ui->lbl_port_status->setText("Port close");
            return;
        }
        m_comPort->setPortName(QString(ui->cmb_port_name->currentText()));
        m_comPort->open(QIODevice::ReadWrite);
        if (m_comPort->isOpen())
            ui->lbl_port_status->setText("Port open");
        else
            ui->lbl_port_status->setText("Port not open");

        m_comPort->setDtr(false);
        m_comPort->setRts(false);
}

void MainWindow::receiveMsgpack(QByteArray data) {

    gps_data = data;
    QString nmea = QString(gps_data.data());
    //qDebug() << nmea << endl;

    int idx_start = nmea.indexOf("RMC");
    if (idx_start != -1) {
        int idx_stop = nmea.indexOf("\r\n", idx_start);
        QString rms_line = nmea.mid(idx_start - 3, idx_stop - idx_start + 3);
        int idx_star = rms_line.indexOf("*");
        QString chsum    = rms_line.mid(idx_star + 1);
        qDebug() << rms_line <<" chsum = " << chsum  << endl;

        // checked cumsum
        QByteArray ba = rms_line.toLocal8Bit();
        char *rms = ba.data();
        int ch = getCheckSum(rms);
        bool bStatus = false;
        uint chsum_true = chsum.toUInt(&bStatus, 16);
        if (bStatus & (chsum_true == ch)) {
            socket->writeDatagram(rms_line.toUtf8(), QHostAddress::Broadcast, socket_port);
            qDebug() << chsum_true <<"_"<< ch << "_" << bStatus  << endl;
        }
    }

    QFile f("NMEA.log");
    if (f.open(QIODevice::WriteOnly | QIODevice::Append)) {
        f.write(gps_data.data());
    }
    f.close();
}

int MainWindow::getCheckSum(char *string) {
    int i;
    int XOR;
    int c;
    // Calculate checksum ignoring any $'s in the string
    for (XOR = 0, i = 0; i < strlen(string); i++) {
        c = (unsigned char)string[i];
        if (c == '*') break;
        if (c != '$') XOR ^= c;
    }
    return XOR;
}

void MainWindow::SendNMEAfromFile(int value) {
    if (value == 2) { //checked
        timer->start(500);
    } else {
        timer->stop();
    }
}

void MainWindow::TimerSend() {
    QString rms_line = "sss";
    socket->writeDatagram(rms_line.toUtf8(), QHostAddress::Broadcast, socket_port);
}
