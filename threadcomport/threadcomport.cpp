
#include <threadcomport.h>
#include <qextserialport.h>

#include <QtCore>
#include <QtCore/QtDebug>
#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QTime>
#include <QByteArray>


/*
==============
<CONSTRUCTORS>
==============
*/
Qthreadcomport::Qthreadcomport (const PortSettings *settings):
        QextSerialPort(*settings)
{
    thread = NULL;
    ftimeout = 0;
}

ReceiveThread::ReceiveThread()
{
    lastcount = 0;
    ftimeout = 0;
}

QMainComThread::QMainComThread(QString name, const PortSettings *settings)
{
    comsettings = new PortSettings();
    *comsettings = *settings;
    comname = name;
    comport = NULL;
}
/*
==============
<DESTRSTRUCTORS>
==============
*/
Qthreadcomport::~Qthreadcomport()
{
    stopThread();
}
QMainComThread::~QMainComThread()
{
    terminate();
    wait();
    delete comport;
    comport = NULL;
    delete comsettings;
    comsettings = NULL;
}
/*
==============
<METHODS>
==============
*/
/*TMainComThread*/
Qthreadcomport* QMainComThread::getPort()
{
    return comport;
}

void QMainComThread::run()
{
    Qthreadcomport *port = new Qthreadcomport(comsettings);
    port->setPortName(comname);
    comport = port;
    exec();
}

/*QReceiveThread*/
void ReceiveThread::run()
{
    int count;
    QTime timerTimeout;
    timerTimeout.start();
    forever
    {
        msleep(1);
        mutex.lock();
        count = comport->bytesAvailable();
        mutex.unlock();

        if (count > 0)
        {
            if (timerTimeout.elapsed() > ftimeout)
                {
                    emit newDataInPortThread(count);
                    //QTime timedb;
                    timerTimeout.restart();
                }
        }
        if (timerTimeout.elapsed() > ftimeout)
            timerTimeout.restart();
    }
}

void ReceiveThread::setPort(Qthreadcomport *port)
{
    comport = port;
}

/*Qthreadcomport*/
void Qthreadcomport::close()
{
    stopThread();
    QextSerialPort::close();
}

bool Qthreadcomport::open(QIODevice::OpenMode mode)
{
    thread = new ReceiveThread();
    thread->setPort(this);
    connect(thread,SIGNAL(newDataInPortThread(int)),this,SLOT(newDataInPort(int)));
    bool bopen = QextSerialPort::open(mode);
    thread->start();
    thread->setTimeout(ftimeout);
    return bopen;
}

qint64 Qthreadcomport::readData(char *data, qint64 maxSize)
{
    qint64 count = QextSerialPort::readData(data, maxSize);
    return count;
}

void Qthreadcomport::stopThread()
{
    if (thread != NULL)
    {
        thread->terminate();
        thread->wait();
        delete thread;
        thread = NULL;
    }
}

void Qthreadcomport::setTimeout(int timeout)
{
    if (thread != NULL)
        thread->setTimeout(timeout);
    ftimeout = timeout;
}

/*
==============
<SLOTS>
==============
*/

void  Qthreadcomport::newDataInPort(int count) {
    if (count>400)
        this->newDataInPortSlot(count);
}




void Qthreadcomport::newDataInPortSlot(int count)
{
    if (bytesAvailable() == 0 || !(isOpen()))
            return;
    char data[8192];
    readData(data, count);
    QByteArray array;
    array.append(data);
    emit binsPackGot(array);
}


