#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <QObject>
#include <QUdpSocket>
#include <threadcomport.h>
#include "ui_mainwindow.h"

enum TypeDataInOut {DEC_, ASCII_, HEX_, BIN_};

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);

public slots:

private:
    Ui::MainWindow *ui;
    Qthreadcomport *m_comPort;
    QMainComThread *m_comPortThread;
    QUdpSocket *socket;
    QTimer *timer;
    int socket_port;

    QByteArray gps_data;

private slots:
    void port_b_open();
    void receiveMsgpack(QByteArray data);
    int getCheckSum(char *string);
    void SendNMEAfromFile(int);
    void TimerSend();
};

#endif // MAINWINDOW_H
